package twitter.macron.project.service;

import twitter.macron.project.bean.Tweet;

import java.util.List;

/**
 * TwitterService provides interface to manipulate tweets from DB
 */
public interface TwitterService {
    /**
     * Save a tweet in DB
     * @param tweet
     * @return saved Tweet
     */
    Tweet saveTweet(Tweet tweet);

    /**
     * Get Lasts tweets from twitter 4J API
     * @return last tweet list
     */
    List<Tweet> getLastTweets();

    /**
     * Synchronize tweets in DB (save new tweets not in DB).
     * Planned to run every 90 minutes
     */
    void syncTweets();
}
