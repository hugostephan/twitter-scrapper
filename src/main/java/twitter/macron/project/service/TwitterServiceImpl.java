package twitter.macron.project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import twitter.macron.project.bean.Tweet;
import twitter.macron.project.configuration.TwitterConfig;
import twitter.macron.project.repository.TweetRepository;
import twitter4j.*;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * TwitterService Implementation
 */
@Component
public class TwitterServiceImpl implements TwitterService{
    @Autowired
    TweetRepository tweetRepository;

    @Autowired
    TwitterConfig twitterConfig;

    Twitter twitter;

    /**
     * Initialize twitterConfig : connection via twitter 4J API with token
     * @throws TwitterException
     */
    @PostConstruct
    public void init() throws TwitterException {
        this.twitter=twitterConfig.setupTwitter();
    }

    @Override
    public Tweet saveTweet(Tweet tweet) {
        return tweetRepository.save(tweet);
    }

    @Override
    public List<Tweet> getLastTweets() {
        List<Status> statuses = null;
        try {
            Double lat = new Double (48.85341);
            Double lon = new Double(2.3488);
            String resUnit= "mi";
            Query query = new Query().geoCode(new GeoLocation(lat, lon), 5,resUnit);
            query.count(100);
            QueryResult result = twitter.search(query);
            statuses = result.getTweets();



            //statuses = twitter.getHomeTimeline().subList(0,15);
        } catch (TwitterException e) {
            e.printStackTrace();
        }

        List<Tweet> tweetList = new ArrayList<Tweet>();
        for (Status status2 : statuses) {
            Tweet tweet =new Tweet();
            tweet.setMessage(status2.getText());
            tweet.setUsername(status2.getUser().getName());
            tweet.setDate(status2.getCreatedAt());
            tweetList.add(tweet);
        }
        return tweetList;
    }

    @Override
    @Scheduled(fixedRate=900000)
    public void syncTweets() {
        System.out.println("Synchronisation des tweets en base.");
        List<Tweet> lastTweets = this.getLastTweets();
        for(Tweet t : lastTweets)
        {
            if(tweetRepository.findByMessageAndUsername(t.getMessage(),t.getUsername()) == null)
            {
                this.saveTweet(t);
            }
        }
    }
}
