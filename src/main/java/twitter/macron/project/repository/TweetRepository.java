package twitter.macron.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import twitter.macron.project.bean.Tweet;

/**
 * TweetRepository : extends JPA repo and provides classic DB queries.
 */
public interface TweetRepository extends JpaRepository<Tweet,Integer> {
    /**
     * Fetch a tweet based on a message
     * @param message tweet text
     * @return Tweet object with the given message if it exists in DB.
     */
    Tweet findByMessage(String message);

    /**
     * Fetch a tweet based on a message and a username
     * @param message tweet text
     * @param username tweet author
     * @return Tweet object with the given message and from the given author if it exists in DB.
     */
    Tweet findByMessageAndUsername(String message, String username);
}
