package twitter.macron.project.configuration;

import org.springframework.stereotype.Component;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;

import java.util.List;

/**
 * Twitter Config : configure connection to twitter4J API
 */
@Component
public class TwitterConfig {

    /**
     * Initialize connection to Twitter 4J API.
     * @return an instance of a Twitter object allowing to send queries to twitter.
     * @throws TwitterException in case of bad connection.
     */
    public Twitter setupTwitter() throws TwitterException {
        TwitterFactory factory = new TwitterFactory();
        AccessToken accessToken = loadAccessToken(0);
        Twitter twitter = factory.getInstance();
        twitter.setOAuthConsumer("UH49EBsjotXBmJUkNoX2n3YbJ", "DUPzuUeP50k2Mmq0Uq2OF7jovkmIKLeJHTLS4fGq45gdwWDtIA");
        twitter.setOAuthAccessToken(accessToken);

        return twitter;
    }

    /**
     * Generate single acces token to connect to twitter via 4J API
     * @param useId
     * @return
     */
    private static AccessToken loadAccessToken(int useId){
        String token = "356475990-eM6Pi1Wc8g7BMCWuV1D5mC9RrFzsMBF1oUjmM3P5"; // load from a persistent store
        String tokenSecret ="Wu1XHdm4Rl77JZi6bpo1Y1J5f9srPc3PT85xPWTm32HJK"; // load from a persistent store
        return new AccessToken(token, tokenSecret);
    }
}
