package twitter.macron.project.bean;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Tweet Entity : represents a tweet message : id, message, (creation) date, (author) username
 */
@Entity
public class Tweet {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;
    private String message;
    private Date date;
    private String username;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
